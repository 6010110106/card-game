import React, { Component } from 'react'
import WordCard from './WordCard'
import RandomWord from 'random-words'

const dimOpacity = (i, bound) => {
	return ((bound-i) / bound)
}
export default class RandomWordCard extends Component {
	constructor(props) {
		super(props)
		let w = RandomWord({
			maxLength: 7, 
			exactly: 1
		}).toString().toUpperCase()
		this.state = {
			randomWord: w, 
			bound: (Math.round(1.5*w.length)),
			guessed: [], 
			solution: ''
		}
	}

	guessAttemptHandler = (guess) => {
		if (guess === this.state.randomWord) {
			// correct guess
			this.setState({solution: guess})
		}
		else {
			// incorrect guess
			let guessed = [guess, ...this.state.guessed]
			this.setState({guessed: guessed.slice(0, this.state.bound)})
		}
	}

	render() {
		return (
			<div>
				<WordCard word={this.state.randomWord} 
					guessAttemptHandler={this.guessAttemptHandler}
					display={false}
				/>
				<WordCard word={this.state.solution}
					display={true} solution={true}
				/>
				<div id='pastAttempts'>
				{
					this.state.guessed.map((word, i) => 
					<div key={i}
						style={{opacity: dimOpacity(i, this.state.bound)}}>
						<WordCard word={word} 
							display={true} solution={false} />
					</div>)
				}
				</div>
				
			</div>
		)
	}
}