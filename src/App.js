import React from 'react';
//import logo from './logo.svg';
import './App.css';
//import CharacterCard from './CharacterCard'
//import WordCard from './WordCard'
import RandomWordCard from './RandomWordCard'

function App() {
    return ( 
        <div className="App">
            <RandomWordCard/>
        </div>
    );
}

export default App;