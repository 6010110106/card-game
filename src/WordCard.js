import React, { Component } from 'react'
import _ from 'lodash'
import CharacterCard from './CharacterCard'

const wordStateInit = (input_word) => {
    let word = input_word.toUpperCase()
    let chars = _.shuffle(Array.from(word))
    return {
        word, chars, attempt: 1, guess: [], completed: false
    }
}

const setWord = (word, shuffle=false) => {
    word = word.toUpperCase()
    let chars = Array.from(word)
    if (shuffle) {
        chars = _.shuffle(chars)
    }
    return { word, chars }
}

export default class WordCard extends Component {
    constructor(props) {
        super(props)
        let pending = wordStateInit(this.props.word)
        let wordChars = setWord(this.props.word, !this.props.display)
        this.state = { ...pending, ...wordChars }
    }
    activationHandler = c => {
        let guess = [...this.state.guess, c]
        this.setState({guess})
        if (guess.length === this.state.chars.length) {
            let guessed = guess.join('').toString()
            if (typeof this.props.guessAttemptHandler === 'function') {
                this.props.guessAttemptHandler(guessed)
            }
            if (guessed === this.state.word) {
                this.setState({guess: [], completed: true})
            }
            else {
                this.setState({guess: [], attempt: this.state.attempt+1})
            }
        }
    }
    noop = (x) => {}
    render() {
        return (
            <div className={this.props.display? 'disabledCard': ''}>
                {
                    this.state.chars.map((c, i) => 
                        <CharacterCard value={c} key={i} 
                        activationHandler={this.props.display?
                        this.noop:this.activationHandler}
                        attempt={this.state.attempt} 
                        completed={this.state.completed}
                        display={this.props.display}
                        solution={this.props.solution} />
                    )
                }
            </div>
        );
    }
    componentDidUpdate(prevs) {
        if (prevs.word !== this.props.word) {
            this.setState(setWord(this.props.word, !this.props.display))
        }
    }
}