import React, { Component } from 'react'

const cardState = (state) => {
    let classes = ['card']
    if (state.solution && state.display) {
        classes.push('solutionCard')
    }
    else if (!state.solution && state.display) {
        classes.push('dimmedCard')
    }
    if (state.active) {
        classes.push('activeCard')
    }
    if (state.complete) {
        classes.push('completedCard')
    }
    return classes.join(' ').toString()
}

export default class CharacterCard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            active: false,
            completed: false
        }
    }

    activate = () => {
        if (this.props.display === true) {
            return
        }
        if (!this.state.active) {
            if (typeof this.props.activationHandler === "function") {
                this.props.activationHandler(this.props.value)
            }
            this.setState({ active: true })
        }
    }

    render() {
        let className = cardState(
            {
                solution: this.props.solution, 
                active: this.state.active, 
                complete: this.state.completed,
                display: this.props.display
            }
        )
        return (
            <div className={className} onClick={this.activate}>
                {this.props.value}
            </div>
        )
    }

    componentDidUpdate(prevprops) {
        if (prevprops.attempt !== this.props.attempt) {
            this.setState({ active: false })
        }
        if (prevprops.completed !== this.props.completed) {
            this.setState({ completed: true })
        }
    }
}